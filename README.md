# Mezos — Middleware Tezos for Mobile Devices and more

# Introduction and rationale

The aim of Mezos is to provide a featureful API to Tezos applications.
In particular:

* Block explorer
* REST Wallet

The Tezos command line client unfortunately does not come with RPCs
(but might, in the future). This is a common need for Tezos
applications.

Block explorer facitily is also a common need for applications, for
example Tezos clients need to display an history of transactions
involving a particular address.

# Block explorer

It is implemented using SQLite, leveraging the great `ocaml-sqlexpr`
library. SQL statements are written using a PPX extension and are
compiled into OCaml functions that directly perform SQL operations
transparently. See `Chain_db` module. For now, only relevant info for
clients are stored in the DB. In the future, it could be more
complete. Also the SQL schema can probably be optimized.

# REST wallet

The REST wallet does two things:

* Provide REST access to `tezos-client`.
* Add a local BIP32 wallet.

The first feature is implemented with the `tezos-client` libs, using
`ocplib-resto` for implementing the REST server and services as it is
done in Tezos.

The second feature leverages `ocaml-bip32-ed25519`, itself built on
`ocaml-monocypher`, a neat cryptographic library. This has been done
as a temporary measure since BIP32 was not a standard feature of the
Tezos client yet.

With Mezos, it is possible to generate a mnemonic, encrypt it in a
SQLite DB, and then derive arbitrary addresses using the BI32-Ed25519
scheme. This feature is useful if you want for example host a web
service that need to create a new Tezos address for each customer.

## How signing is performed

This software uses the remote signing scheme of Tezos client in order
to be able to perform its own signature. It implements a local socket
signer and instruct the Tezos client functions to use it as its
signer. Upon receiving signing requests from Tezos-client, it will
sign them using either:

* its local BIP32 wallet
* remote clients, that will perform signature themselves

In the latter case, there is a two-step REST API directed towards the
remote client. The first API call (`/transfer/init`) is used by the
remote client to request a transfer, whereas the second call
(`/transfer/finalize`) is used to finalize the transfer: the remote
client will provide the signature.

# Roadmap

* Use `tezos-clic` instead of Cmdliner for client commands (low priority)
* Improve chain bootstrapping (still fragile, sometimes buggy)
* Avoid leaking resources for the mobile signing functions