(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Rresult
open Alpha_environment
open Alpha_context

let k_arg =
  RPC_arg.make
    ~descr: "Contract"
    ~name: "contract"
    ~destruct:(fun s ->
        R.reword_error
          (fun e -> Format.asprintf "%a" pp_print_error e)
          (wrap_error (Contract.of_b58check s)))
    ~construct:Contract.to_b58check ()

let pkh_arg =
  RPC_arg.make
    ~descr: "Public key hash"
    ~name: "pkh"
    ~destruct:(fun s ->
        R.reword_error
          (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
          (R.trap_exn Signature.Public_key_hash.of_b58check_exn s))
    ~construct:Signature.Public_key_hash.to_b58check ()

let ks_query =
  let open RPC_query in
  query (fun a -> a)
  |+ multi_field
    ~descr:"Contracts to get history for" "ks" k_arg (fun a -> a)
  |> seal

let history =
  RPC_service.get_service
    ~description: "Get the history of contracts"
    ~query:ks_query
    ~output:Data_encoding.(list (list Chain_db.tx_full_encoding))
    RPC_path.(root / "history")

let contracts =
  let query =
    let open RPC_query in
    query (fun a -> a)
    |+ multi_field ~descr:"pkhs" "mgrs" pkh_arg (fun a -> a)
    |> seal in
  RPC_service.get_service
    ~description: "Get contracts managed by mgr"
    ~query
    ~output:Data_encoding.(list (list Contract.encoding))
    RPC_path.(root / "contracts")

let contract_info =
  RPC_service.get_service
    ~description: "Get delegate status of contracts"
    ~query:ks_query
    ~output:Data_encoding.(list (option Chain_db.Contract_table.json_encoding))
    RPC_path.(root / "contract_info")

let missing_blocks =
  RPC_service.get_service
    ~description: "Find missing blocks in DB"
    ~query:RPC_query.empty
    ~output:Data_encoding.(list int32)
    RPC_path.(root / "missing_blocks")

let stake =
  RPC_service.get_service
    ~description:"Query Stakes DB"
    ~query:RPC_query.empty
    ~output:Data_encoding.(list Mezos_stake.fulldiff_encoding)
    RPC_path.(root / "stake" /: pkh_arg /: k_arg )
