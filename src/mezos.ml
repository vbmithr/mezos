(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Cmdliner
open Alpha_cmdliner_helpers

let src =
  Logs.Src.create ~doc:"Main Mezos daemon" "mezos"
let log_src = src

let default_remote_host = "localhost"
let default_tezos_rpc_port = 8732
let default_tezos_zeronet_rpc_port = 18732
let default_mezos_rpc_port = 8776

let default_tezos_zeronet_url =
  Uri.make ~scheme:"http" ~host:default_remote_host ~port:default_tezos_zeronet_rpc_port ()
let default_tezos_url =
  Uri.make ~scheme:"http" ~host:default_remote_host ~port:default_tezos_rpc_port ()
let default_mezos_url =
  Uri.make ~scheme:"http" ~host:default_remote_host ~port:default_mezos_rpc_port ()

let cleanup_files = ref []
let () =
  Sys.set_signal Sys.sigint begin Signal_handle begin fun _ ->
      List.iter begin fun fn ->
        Logs.info ~src (fun m -> m "Cleaned up %s" fn) ;
        Unix.unlink fn ;
      end !cleanup_files ;
      exit 0
    end
  end

module IntSet = Set.Make(Int32)

(* let missing_blocks db =
 *   Chain_db.fold_levels db begin fun a level _hash ->
 *     Lwt.return (IntSet.add level a)
 *   end IntSet.empty >>= fun levels ->
 *   match IntSet.max_elt_opt levels with
 *   | None -> Lwt.return IntSet.empty
 *   | Some max_elt ->
 *     let rec acc_missing missing i =
 *       if i >= max_elt then missing
 *       else
 *       if not (IntSet.mem i levels) then
 *         acc_missing (IntSet.add i missing) (Int32.succ i)
 *       else missing in
 *     Lwt.return (acc_missing IntSet.empty 3l) *)

module Directories = struct
  let forge_transfer cctxt dir =
    let open RPC_directory in
    register0 dir Mezos_services.forge_transfer
      begin fun () ({ src; src_pk; ops } as t) ->
        Logs_lwt.info begin fun m -> m "<- %a"
            Mezos_transfer.(pp_manager_operations pp_destination) t
        end >>= fun () ->
        Mezos_transfer.forge_transfer cctxt src ~pk:src_pk ops
      end

  let originate_account cctxt dir =
    let open RPC_directory in
    register0 dir Mezos_services.originate_account
      begin fun () { src; src_pk; ops } ->
        Mezos_transfer.forge_origination cctxt src src_pk ops
      end

  let change_delegate cctxt dir =
    let open RPC_directory in
    register0 dir Mezos_services.change_delegate
      begin fun () { src; src_pk; ops } ->
        match ops with
        | [] ->  Mezos_transfer.forge_delegation cctxt src src_pk None
        | pkh :: _ -> Mezos_transfer.forge_delegation cctxt src src_pk (Some pkh)
      end

  let history chain_db dir =
    let open RPC_directory in
    register0 dir Mezos_services_common.history begin fun ks () ->
      Chain_db.history chain_db ks >>|? fun txs ->
      List.map begin fun txs ->
        Operation_hash.Map.fold begin fun _oh os a ->
          Chain_db.IntMap.fold begin fun _op_id tx a ->
            tx :: a
          end os a
        end txs []
      end txs
    end

  let contracts conn dir =
    let open RPC_directory in
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    register0 dir Mezos_services_common.contracts begin fun pkhs () ->
      Lwt_list.map_s begin fun pkh ->
        let open Chain_db.Contract_table in
        Conn.collect_list select_by_mgr pkh >>=
        Caqti_lwt.or_fail >|= fun l ->
        List.map (fun { k ; _ } -> k) l
      end pkhs >>= fun kss ->
      return kss
    end

  let contract_info conn dir =
    let open RPC_directory in
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    register0 dir Mezos_services_common.contract_info begin fun ks () ->
      let open Chain_db.Contract_table in
      Lwt_list.map_s begin fun k ->
        Conn.find_opt select_by_k k >>=
        Caqti_lwt.or_fail
      end ks >>= fun infos ->
      return infos
    end

  (* let missing_blocks chain_db dir =
   *   let open RPC_directory in
   *   register0 dir Mezos_services_common.missing_blocks begin fun () () ->
   *     missing_blocks chain_db >>= fun missing ->
   *     return (IntSet.elements missing)
   *   end *)

  let stake conn dir =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    let open RPC_directory in
    let open Chain_db.Stake_table in
    register2 dir Mezos_services_common.stake begin fun delegate k () () ->
      Conn.fold select_by_delegate_and_k
        begin fun { delegate = _ ; level; contract = _ ; kind; amount } a ->
          Mezos_stake.create_fulldiff ~level ~kind ~amount :: a
        end (delegate, k) [] >>=
      Caqti_lwt.or_fail >>= fun evts ->
      return (List.rev evts)
    end
end

let run tezos_url tezos_client_dir wallet_db chain_db mezos_url () =
  Tezos_sql.connect wallet_db >>= fun wallet_conn ->
  Tezos_sql.connect chain_db >>= fun chain_conn ->
  let module Wallet_conn = (val wallet_conn : Caqti_lwt.CONNECTION) in
  let module Chain_conn = (val chain_conn : Caqti_lwt.CONNECTION) in
  Tezos_cfg.mk_rpc_cfg
    tezos_client_dir tezos_url >>=? fun (rpc_config, confirmations) ->
  let cctxt = new wrap_full (Tezos_cfg.mezos_full ~rpc_config ?confirmations ()) in
  let mezos_url =
    Option.unopt ~default:default_mezos_url mezos_url in
  let host = Option.unopt ~default:"localhost" (Uri.host mezos_url) in
  let mezos_port = Option.unopt ~default:8776 (Uri.port mezos_url) in
  RPC_server.launch ~host ~media_types:Media_type.all_media_types (`TCP (`Port mezos_port))
    begin
      RPC_directory.register_describe_directory_service begin
        RPC_directory.empty |>
        Directories.forge_transfer cctxt |>
        Directories.originate_account cctxt |>
        Directories.change_delegate cctxt |>
        Directories.contract_info chain_conn |>
        Directories.history chain_conn |>
        Directories.contracts chain_conn |>
        (* Directories.missing_blocks chain_db |> *)
        Directories.stake chain_conn
      end
        RPC_service.description_service
    end
  >>= fun _server ->
  let th, _u = Lwt.wait () in
  th >>= fun () ->
  return_unit

let accounts db () =
  Tezos_sql.connect db >>= fun conn ->
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let open Chain_db.Contract_table in
  let open Chain_db.AC in
  Conn.fold select begin fun { k; manager; _ } () ->
    Logs.app ~src begin fun m -> m "%a %a"
        Contract.pp k Signature.Public_key_hash.pp manager
    end
  end () () >>=
  Caqti_lwt.or_fail >>= fun () ->
  return_unit

let remote_history base srcs max_level () =
  let open Alpha_context in
  let base =
    Option.unopt ~default:default_mezos_url base in
  RPC_client.call_service Media_type.all_media_types
    ~base Mezos_services_common.history () srcs () >>=? fun txs ->
  List.iter2 begin fun cur_src txs ->
    let open Chain_db.AC in
    let module KMap = Map.Make(Contract) in
    let summary =
      List.fold_left begin fun a { Chain_db.src; dst; amount; level; _ } ->
        if level > max_level then a else
          match cur_src = src, cur_src = dst with
          | true, true -> a (* delegate to itself *)
          | true, false -> (* out *)
            KMap.update dst begin function
              | None -> (match Tez.(zero -? amount) with Ok v -> Some v | _ -> None)
              | Some c -> (match Tez.(c -? amount) with Ok v -> Some v | _ -> None)
            end a
          | false, true -> (* in *)
            KMap.update src begin function
              | None -> Some amount
              | Some c -> (match Tez.(c +? amount) with Ok v -> Some v | _ -> None)
            end a
          | false, false ->
            invalid_arg "remote_history"
      end KMap.empty txs in
    Logs.app ~src (fun m -> m "For %a" Contract.pp cur_src) ;
    let sum =
      KMap.fold (fun _ t a -> Int64.add a (Tez.to_mutez t)) summary 0L in
    KMap.iter begin fun k t ->
      let part = Tez.to_mutez t in
      let p = Int64.(to_float part /. to_float sum) in
      Logs.app ~src (fun m -> m "%a %a %f" Contract.pp k Tez.pp t p)
    end summary
  end srcs txs ;
  return_unit

let blkid_of_hash_level hash level =
  `Main, `Hash (hash, level)

open Cmdliner_helpers.Convs
open Cmdliner_helpers.Terms
module OldConvs = Old_alpha_cmdliner_helpers.Convs
module OldTerms = Old_alpha_cmdliner_helpers.Terms
module NewConvs = Alpha_cmdliner_helpers.Convs
module NewTerms = Alpha_cmdliner_helpers.Terms

let default_chain_url =
  Uri.make ~scheme:"postgresql" ~host:"localhost" ~path:"chain" ()
let default_wallet_url =
  Uri.make ~scheme:"postgresql" ~host:"localhost" ~path:"wallet" ()

let run_cmd =
  let doc = "Run a Mezos node." in
  let tezos_url =
    uri_option ~args:["tezos-url"] ~doc:"URL of a running Tezos node" () in
  let mezos_url =
    uri_option ~args:["mezos-url"] ~doc:"URL to bind Mezos" () in
  Term.(const run $
        tezos_url $
        tezos_client_dir $
        uri ~default:default_wallet_url ~doc:"Wallet DB" ~args:["wallet-db"] $
        uri ~default:default_chain_url ~doc:"Chain DB" ~args:["chain-db"] $
        mezos_url $
        setup_log),
  Term.info ~doc "run"

let history_cmd =
  let doc = "History of an account" in
  let ks =
    Arg.(value & pos_right ~-1 OldConvs.contract [] &
         info [] ~docv:"CONTRACT" ~doc) in
  let history db ks () =
  Tezos_sql.connect db >>= fun conn ->
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    Chain_db.history ~display:true conn ks >>|? ignore in
  Term.(const history $
        db ~default:default_chain_url $
        ks $
        setup_log),
  Term.info ~doc "history"

let history_remote_cmd =
  let doc = "History of an account using RPC calls" in
  let max_level =
    Arg.(value & opt int max_int & info ["max-level"] ~docv:"LEVEL") in
  let src =
    Arg.(value & pos_right ~-1 OldConvs.contract [] & info [] ~docv:"SRC") in
  Term.(const remote_history $
        uri_option ~args:["mezos-url"] ~doc: "URL of a Mezos server" () $
        src $ max_level $ setup_log),
  Term.info ~doc "history-remote"

let accounts_cmd =
  let doc = "List all accounts on the blockchain" in
  Term.(const accounts $
        db ~default:default_chain_url $
        setup_log),
  Term.info ~doc "accounts"

let lwt_run v =
  Lwt.async_exception_hook := begin fun exn ->
    Logs.err ~src (fun m -> m "%a" pp_exn exn) ;
  end ;
  match Lwt_main.run v with
  | Error err ->
    Logs.err ~src (fun m -> m "%a" pp_print_error err) ;
    exit 1
  | Ok () -> ()

let cmds =
  List.map begin fun (term, info) ->
    Term.((const lwt_run) $ term), info
  end [
    accounts_cmd ;
    history_cmd ;
    history_remote_cmd ;
    run_cmd ;
  ]

let default_cmd =
  let doc = "Mezos: high level node/client on top of Tezos" in
  Term.(ret (const (`Help (`Pager, None)))),
  Term.info ~doc "mezos"

let () = match Term.eval_choice default_cmd cmds with
  | `Error _ -> exit 1
  | #Term.result -> exit 0
